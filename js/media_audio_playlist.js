/**
 * @file
 * Attaches playlist javascript.
 * This is copied almost entirely wholesale from:
 *   http://devblog.lastrose.com/html5-audio-video-playlist/
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  $(document).ready(function () {

    var audio;
    var current;
    var len;
    var link;
    var par;
    var playlist;
    var tracks;

    init();
    function init() {
      current = 0;
      audio = $('audio');
      playlist = $('#playlist');
      tracks = playlist.find('li a');
      len = tracks.length - 1;
      audio[0].volume = .10;
      playlist.find('a').click(function(e){
        e.preventDefault();
        link = $(this);
        current = link.parent().index();
        run(link, audio[0]);
      });
      audio[0].addEventListener('ended',function(e){
        current++;
        if(current == len){
          current = 0;
          link = playlist.find('a')[0];
        }else{
          link = playlist.find('a')[current];
        }
        run($(link),audio[0]);
      });
    }
    function run(link, player) {
      player.src = link.attr('href');
      par = link.parent();
      par.addClass('active').siblings().removeClass('active');
      audio[0].load();
      audio[0].play();
    }

  });

})(jQuery, Drupal, drupalSettings);
