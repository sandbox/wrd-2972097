<?php

namespace Drupal\media_audio_playlist\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Audio Playlist (basic)' formatter.
 *
 * @FieldFormatter(
 *   id = "audio_playlist",
 *   label = @Translation("Audio playlist"),
 *   description = @Translation("Display multiple files using an HTML5 audio tag in a playlist format."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class AudioPlaylistBasic extends AudioPlaylistBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    unset($settings['multiple_file_display_type']);
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'controls' => [
        '#title' => $this->t('Show playback controls'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('controls'),
      ],
      'autoplay' => [
        '#title' => $this->t('Autoplay'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('autoplay'),
      ],
      'loop' => [
        '#title' => $this->t('Loop'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('loop'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Playback controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('visible') : $this->t('hidden')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Format as playlist
    $playlist = [
      '#theme' => 'media_audio_playlist_formatter',
      '#tracks' => [
        '#theme' => 'item_list',
        '#title' => NULL,
        '#list_type' => 'ul',
        '#attributes' => [
          'id' => 'playlist',
        ],
        '#items' => [],
      ],
      '#first_track' => NULL,
      '#attached' => [
        'library' => [
          'media_audio_playlist/playlist',
        ],
      ],
      '#extravalue' => 'controls',
    ];
    $first = TRUE;
    foreach ($this->getEntitiesToView($items, $lancode) as $delta => $file) {
      $item = $file->_referringItem;
      $link_text = $item->description ? $item->description : $file->getFilename();
      $link_url = Url::fromUri(file_create_url($file->getFileUri()));
      $playlist['#tracks']['#items'][$delta] = [
        '#type' => 'markup',
        '#markup' => Link::fromTextAndUrl(t($link_text), $link_url)->toString(),
      ];
      if ($first) {
        $playlist['#tracks']['#items'][$delta]['#wrapper_attributes'] = [
          'class' => [
            'active',
          ],
        ];
        $playlist['#first_track'] = file_create_url($file->getFileUri());
      }
      $first = FALSE;
    }

    return $playlist;

  }

}
