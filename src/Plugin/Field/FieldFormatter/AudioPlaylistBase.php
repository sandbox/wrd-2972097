<?php

namespace Drupal\media_audio_playlist\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileAudioFormatter;

/**
 * Base class for playlist formatter for audio fields.
 */
abstract class AudioPlaylistBase extends FileAudioFormatter {

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    return parent::getEntitiesToView($items, $langcode);
  }

}
