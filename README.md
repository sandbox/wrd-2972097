## About Media Audio Playlist

This module adds a field formatter for multi-value audio file fields to render them as a simple playlist.

###How to install:
1. Download this module.
2. Install it in the [usual way](https://www.drupal.org/documentation/install/modules-themes/modules-8).

###Project page:
[drupal.org project page](https://www.drupal.org/project/dropzonejs)

###Maintainers:
+ Bill Dickson (@wrd) drupal.org/u/wrd

###Thanks:
 This module was developed for and made available for release by the [State of Missouri](https://mo.gov).
